# Active Energy Data Publication

REGIMO - Registration of Metadata


## Use Cases

### Use Case #1 (Main use case)
A user plugs in the EDR at a certain location, connects it with its laptop and starts the recording of a campaign.
At the same time the user starts the publishing tool REGIMO (from this git repository).
All new created files are tracked by the tool and automatically uploaded to the cloud and then published to the databus using its cloud URL.


### Use Case #2 (Secondary use case)
The existing database of the EDR is analyzed and all existing valid data sets (correct filesystem structure and metadata json files exist) get published to the cloud and registered in the databus.


## File Structure
```
data
├─ <campaign-label>
│  ├─ campaign.json
│  ├─ <measurement-label-1>
│  │  ├─ measurement.json
│  │  ├─ <channel-label>
│  │  │  ├─ channel.json
│  │  │  ├─ <data-start-date-1>.csv or <data-1>.csv
│  │  │  ├─ <data-start-date-2>.csv or <data-2>.csv
│  │  │  ├─ ...
│  │  │  └─ <data-start-date-n>.csv or <data-n>.csv
│  │  ├─ ...
│  │  └─ <channel-label-n>
│  │     ├─ channel.json
│  │     ├─ <data-start-date-1>.csv or <data-1>.csv
│  │     ├─ <data-start-date-2>.csv or <data-2>.csv
│  │     ├─ ...
│  │     └─ <data-start-date-n>.csv or <data-n>.csv
│  ├─ ...
│  └─ <measurement-label-n>
│     ├─ <channel-label>
│     │  ├─ channel.json
│     │  ├─ <data-start-date-1>.csv or <data-1>.csv
│     │  ├─ <data-start-date-2>.csv or <data-2>.csv
│     │  ├─ ...
│     │  └─ <data-start-date-n>.csv or <data-n>.csv
│     ├─ ...
│     └─ <channel-label-n>
│        ├─ channel.json
│        ├─ <data-start-date-1>.csv or <data-1>.csv
│        ├─ <data-start-date-2>.csv or <data-2>.csv
│        ├─ ...
│        └─ <data-start-date-n>.csv or <data-n>.csv
└─ <campaign-label-2>
   └─ ...
```

This structure exists on the current storage solution for the measurement data.
But without the metadata json files for now.



### campaign.json
```json
{
    "device_id": "xyz",
    "subdevice_id": "abc",
    "start_time": "2024-01-16T14:37:02.18384Z",
    "end_time": "2024-01-31T23:59:59.99999Z",
    "location": {
        "type": "Point",
        "coordinates": [100.0, 0.0]
    },
    "campaign_uid": "???"
}
```

> Do we need to update the json files with the UIDs from the databus once the publishing has been done?

Most likely yes


### measurement.json
```json
{
    "start_time": "",
    "end_time": "",
    "status": "waiting to start|running|recording finished|uploaded|published"
}
```

### channel.json
```json
{
    "description": "The frequency of the power grid at the measurement location",
    "unit_label": "Hz",
    "unit_uri": "http....",
    "physical_quantity": "frequency"
}
```


### UIDs

Erstellt erst beim publishen auf Databus




## URL structuring

```
{protocol}://{hostname}:{port}/{path}
```

Protocol: https
Hostname: "cloud bw ..."
Port: "" (443)
Path: Abhängig von Speicherlösung


## Workflow

1) Discover filesystem changes
2) Generate metadata string for new files
3) Upload the file to the cloud to receive a fixed URL for each dataset (only use case #1)
4) Publish metadata string for each new entry on databus
5) Update Json files with UIDs (optional)



# Ideas

## EDR file rotation
The EDR is likely to not just produce a single file for a channel in a measurement but will likely have some rotation process. 
That is reflected in the file structure mentioned above.

## Tracking of the finished state of a campaign / measurement

Either we can expect a certain file to be created once the EDR process stops to write data.

Or we can check the "status" field of the measurement json file to wait for the "recording completed" status.