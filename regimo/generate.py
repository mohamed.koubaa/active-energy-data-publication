import json
import rdflib
import pyshacl
import os

def read_json_file(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        data = json.load(file)
    return data

def generate_jsonld(payload_file_path, account_url, group_name, group_desc, artefact_name, artefact_desc, version_name, version_desc, license_url, download_url):
    """
    Generates structured JSON-LD data for groups, artefacts, and versions from scratch.

    Args:
        account_url (str): The URL of the account hosting the data.
        group_name (str): The name of the group to which the artefact belongs.
        group_desc (str): The description of the group.
        artefact_name (str): The name of the artefact.
        artefact_desc (str): The description of the artefact.
        version_name (str): The date of the dataset's version.
        version_desc (str): The description of the version of the dataset.
        license_url (str): The URL of the license under which the artefact is published.
        download_url (str): The URL where the artefact can be downloaded.

    Returns:
        A JSON string that represents the structured data in JSON-LD format.
    """
    data = {
        "@context": "https://downloads.dbpedia.org/databus/context.jsonld",
        "@graph": [
            {
                "@id": f"{account_url}/{group_name.replace(' ', '_')}",
                "@type": "Group",
                "title": group_name,
                "abstract": group_desc,
                "description": group_desc
            },
            {
                "@id": f"{account_url}/{group_name.replace(' ', '_')}/{artefact_name.replace(' ', '_')}",
                "@type": "Artifact",
                "title": artefact_name,
                "abstract": artefact_desc,
                "description": artefact_desc
            },
            {
                "@type": ["Version", "Dataset"],
                "@id": f"{account_url}/{group_name.replace(' ', '_')}/{artefact_name.replace(' ', '_')}/{version_name}",
                "hasVersion": version_name,
                "title": artefact_name,
                "abstract": version_desc,
                "description": version_desc,
                "license": license_url,
                "distribution": [
                    {
                        "@type": "Part",
                        "formatExtension": "md",
                        "compression": "none",
                        "downloadURL": download_url
                    }
                ]
            }
        ]
    }
    with open(payload_file_path, 'w') as json_file:
        json.dump(data, json_file, indent=2)

    return json.dumps(data, indent=2)

def validate_jsonld(jsonld_file_path, shacl_schema_file_path):
    """
    Validates JSON-LD data against a SHACL schema file and prints the validation result.

    Args:
        jsonld_file_path (str): Path to the JSON-LD file.
        shacl_schema_file_path (str): Path to the SHACL schema file.
    """

    with open(jsonld_file_path, 'r') as json_file:
        jsonld_data = json.load(json_file)

    
    with open(shacl_schema_file_path, 'r') as shacl_file:                               # Load SHACL shape from external file
        shacl_shape = shacl_file.read()

    jsonld_graph = rdflib.Graph().parse(data=json.dumps(jsonld_data), format='json-ld') # Convert JSON-LD-File in RDFLib-Graph
    shacl_graph = rdflib.Graph().parse(data=shacl_shape, format='turtle')               # Convert SHACL-Shape in RDFLib-Graph

    conforms, report_graph, report_text = pyshacl.validate(jsonld_graph, shacl_graph, inference='rdfs', abort_on_first=False, meta_shacl=False)     # Validate RDF data with SHACL-Shape

    if conforms:                                                                         # Returns the validation result
        print("The JSON-LD data complies with the SHACL rules.")
    else:
        print("The JSON-LD data does not comply with the SHACL rules. Error:")
        print(report_text)
        for result in report_graph:
            print(result)

def process_json_data(json_data, payload_file_path, account_url):
    # Initialise the variables before the loop
    group_name = 'Nicht vorhanden'
    group_desc = 'Nicht vorhanden'
    artefact_name = 'Nicht vorhanden'
    artefact_desc = 'Nicht vorhanden'
    version_name = 'Nicht vorhanden'
    version_desc = 'Nicht vorhanden'
    license_url = 'Nicht vorhanden'
    download_url = 'Nicht vorhanden'

    campaigns = json_data.get('campaign', [])
    for campaign in campaigns:
        group_name = campaign.get('name')
        group_desc = campaign.get('description')

    measurements = json_data.get('measurement', [])
    for measurement in measurements:
        artefact_name = measurement.get('name')
        artefact_desc = measurement.get('description')

    channels = json_data.get('channel', [])
    for channel in channels:
        version_name = channel.get('name')
        version_desc = channel.get('description')  
        download_url = channel.get('pathID')  # Permalink for dataset


    licenses = json_data.get('licenses', [])
    for license in licenses:
        license_url = license.get('path')  


    generate_jsonld(payload_file_path, account_url, group_name, group_desc, artefact_name, artefact_desc, version_name, version_desc, license_url, download_url)
    validate_jsonld(payload_file_path, shacl_schema_file_path)

if __name__ == "__main__":
    personal_path = 'metadata' # Die Datei metadata.json liegt zum Test in der gleichen directory wie publish.jsonld.

    # path for JSON file
    json_file_path = personal_path + '/metadata.json'
    payload_file_path = 'metadata/publish.jsonld'
    shacl_schema_file_path = 'metadata/publish_schema.ttl'

    # Read JSON file
    json_data = read_json_file(json_file_path)

    # Generate JSON-LD data and validate the structure of JSON-LD
    account_url = "https://databus.openenergyplatform.org/moerselj"

    process_json_data(json_data, payload_file_path, account_url)
